<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
    <!--[if lt IE 7]>
      <?php print phptemplate_get_ie_styles(); ?>
    <![endif]-->
  </head>
 <body>
 <?php print $admin ?>
<!-- wrap starts here -->
<div id="wrap">

	<!--header -->
	<div id="header">			
		<div id="header-links">
    <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')); ?>		
		</div>		
		
	<!--header ends-->					
	</div>
		
	<div id="header-photo">
	
		<h1 id="logo-text"><a href="<?php print base_path() ?>" title="home"><?php print $site_name ?></a></h1>		
		<h2 id="slogan"><?php print $site_slogan ?></h2>	
			
	</div>		
			
	<!-- navigation starts-->	
	<div  id="nav">
    <?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
	<!-- navigation ends-->	
	</div>					
			
	<!-- content-wrap starts -->
	<div id="content-wrap" class="three-col"  >	
		<div id="sidebar">
      <?php print $leftside ?>					
		<!-- sidebar ends -->		
		</div>
				<div id="main">
			<div class="tabs"><?php print $tabs ?><?php print $tabs2 ?></div>
			<?php print $content ?>
		</div>
		<div id="rightcolumn">
      <?php print $rightside ?>	
		</div>
		<div class="clear"></div>


		
	<!-- content-wrap ends-->	
	</div>
		
	<!-- footer starts -->			
	<div id="footer-wrap"><div id="footer">				
			
			<p>
			<?php print $footer_message ?>
			</p>
	</div></div>
	<!-- footer ends-->	
	
<!-- wrap ends here -->
</div>
<?php print $closure ?>
</body>
</html>
